package it.antresol.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import it.antresol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends Fragment {

    private static final int[] dataSet = {
            R.drawable.antresol_1,
            R.drawable.antresol_antr,
            R.drawable.antresol_sfhdgshycvgzxhjjzkjalsvmmx,
            R.drawable.antresol_n,
            R.drawable.ololo_antresol,
            R.drawable.znimok_ekrana_antresol,
            R.drawable.screen568_568,
            R.drawable.antresol_dgjhkllkllljhg,
            R.drawable.mieszkanie_z_antresol_7220105420772848335,
            R.drawable.lentochka,
            R.drawable.antresol_dsklfhdsklg
    };
    public static final int SPAN_COUNT = 2;
    private View rootView;
    private RecyclerView feed;
    private FeedAdapter adapter;

    public FeedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_feed, container, false);
        feed = (RecyclerView)rootView.findViewById(R.id.feed);
        feed.setHasFixedSize(true);
        feed.setLayoutManager(new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL));
        adapter = new FeedAdapter();
        feed.setAdapter(adapter);
        return rootView;
    }
    
    private static class FeedAdapter extends RecyclerView.Adapter<ViewHolder>{


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.feed_item, parent, false);
            ViewHolder holder = new ViewHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.image.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(dataSet[position]));
            holder.price.setText("Very very costly UAH");
        }

        @Override
        public int getItemCount() {
            return dataSet.length;
        }
    }
    
    private static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView image;
        final TextView price;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            price = (TextView) itemView.findViewById(R.id.price);
        }
        
    }
}
